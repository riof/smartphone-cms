# Partner CMS - Qoala


### Bitbucket link
```
git remote add origin git@bitbucket.org:volodev/qoala-partner-cms.git  
```

### Development
```
https://dev-alb.qoala.app:9003/
```

### Production
```
http://prod-alb.qoala.app/:9003/
```