module.exports = {
    head: {
        title: 'Qoala Partner Back Office',
        meta: [{
            charset: 'utf-8'
        },
        {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1'
        },
        {
            hid: 'description',
            name: 'description',
            content: 'Back Office'
        }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }]
    },
    loading: {
        color: '#42A5CC'
    },

    loadingIndicator: {
        name: 'circle',
        color: '#fa923f'
    },

    css: [
        '@/node_modules/vuetify/dist/vuetify.css',
        '@/node_modules/vuetify-daterange-picker/dist/vuetify-daterange-picker.css'
    ],

    plugins: [{
        src: '~/plugins/vendor'
    },
    {
        src: '~/plugins/axios',
        ssr: false
    },
    {
        src: '~/plugins/vuetify'
    },
    {
        src: '~/plugins/filters'
    },
    {
        src: '~/plugins/helpers'
    },
    {
        src: '~/plugins/globcomp'
    },
    {
        src: '~/plugins/api'
    }
    ],

    build: {
        // analyze: true
        // extend(config, {
        //     isDev,
        //     isClient
        // }) {
        //     if (isDev && isClient) {
        //         config.module.rules.push({
        //             enforce: 'pre',
        //             test: /\.(js|vue)$/,
        //             loader: 'eslint-loader',
        //             exclude: /(node_modules)/
        //         });
        //     }
        // }
    },

    modules: [
        ['@nuxtjs/axios', {
            baseURL: process.env.BASE_URL || 'https://api-staging.qoala.app/api'
        }],
        ['nuxt-validate', {
            lang: 'en'
        }],
        'nuxt-healthcheck',
        [
            'nuxt-sass-resources-loader',
            [
                '@/assets/styles/style.scss'
            ]
        ]
    ],

    generate: {
        routes: [
            '/',
            '/home',
            '/list',
            '/users',
            '/employee'
        ]
    },

    router: {
        middleware: ['redirect']
    },

    server: {
        port: process.env.PORT || 3000,
        host: process.env.HOST || '0.0.0.0'
    },

    healthcheck: {
        path: '/api/health-check',
        contentType: 'application/json',
        healthy: () => JSON.stringify({
            message: 'success',
            data: {},
            status: 200
        })
    }
};
