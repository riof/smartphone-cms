import Vuex from 'vuex';

import app from './modules/app';
import auth from './modules/auth';
import cmsUser from './modules/cms/users';
import cmsPolicies from './modules/cms/policies';
import cmsClaim from './modules/cms/claims';
import cmsFlight from './modules/cms/flights';

const createStore = () => new Vuex.Store({
    strict: false,
    modules: {
        app,
        auth,
        cmsUser,
        cmsPolicies,
        cmsClaim,
        cmsFlight
    }
});

export default createStore;
