import * as types from '@/store/constants';

export default {
    // userList
    [types.CMS_USER_LIST](state, value) {
        state.users = value;
    },
    // userSingle
    [types.CMS_USER_SINGLE](state, value) {
        state.user = value;
    },
    // updateOneUser
    [types.CMS_USER_DOCUMENT_GET](state, value) {
        state.user.images = {
            ...state.user.images,
            [value.docType]: value.base64
        };
    }
};
