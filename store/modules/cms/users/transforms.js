export default {
    list(data) {
        if (!data.length) return [];

        const determinePriority = (user) => {
            if (user.isBankVerified !== 0 || user.isIdentityVerified !== 0) {
                if (user.claimings.length > 0) {
                    let ct = 0;
                    user.claimings.forEach(claim => {
                        const n = claim.histories.length;
                        if (n > 0) {
                            if (claim.histories[n - 1].status === 'PRE_APPROVED') {
                                ct++;
                            }
                        }
                    });

                    if (ct > 0) {
                        return 'HIGH';
                    }
                }
            }

            return 'NORMAL';
        };

        return data.map(v => {
            const avatar = v.documents.reverse().find(e => e.type === 'profile-image');
            const priority = determinePriority(v);

            return Object.assign(v, {
                avatar: avatar ? avatar.filename : '',
                thumbnail: '',
                policies: v.policies.length,
                documents: [],
                claimings: [],
                priority
            });
        });
    }
};
