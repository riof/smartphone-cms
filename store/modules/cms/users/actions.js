import * as types from '@/store/constants';
import transforms from './transforms';

export default {
    async userList(context, payload) {
        try {
            const response = await this.$api.call('user.list', payload);
            const responseData = transforms.list.call(this, response.data);
            context.commit(types.CMS_USER_LIST, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_USER_LIST, []);
            throw error;
        }
    },

    async userSingle(context, payload) {
        try {
            const response = await this.$api.call('user.single', payload);
            const responseData = response.data;
            context.commit(types.CMS_USER_SINGLE, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_USER_SINGLE, {});
            throw error;
        }
    },

    async userUpdateStatus(context, payload) {
        try {
            const response = await this.$api.call('user.updateStatus', payload);
            const responseData = response.data;
            return responseData;
        } catch (error) {
            throw error;
        }
    }
};
