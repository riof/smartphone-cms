export default {
    list(data) {
        if (!data.length) return [];

        return data.map(v => {
            v.flightDate = null;

            if (v.flight) {
                v.flightDate = v.flight.date;
            }

            if (!v.products || !v.products.length) {
                v.products = [{}];
            }

            return v;
        });
    }
};
