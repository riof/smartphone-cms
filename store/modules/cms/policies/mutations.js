import * as types from '@/store/constants';

export default {
    // policyList
    [types.CMS_POLICY_LIST](state, value) {
        state.policies = value;
    }
};
