export default {
    policies: [
        {
            number: 'TR-01-190110-0X3A0',
            bookingCode: 'TRN025',
            trainName: 'Qoala Super Train',
            trainNumber: '003',
            departureStationCode: 'GMR',
            departureStationName: 'GAMBIR',
            arrivalStationCode: 'WT',
            arrivalStationName: 'WATES',
            scheduledDepartureTime: '2019-01-10T07:30:00.000Z',
            scheduledArrivalTime: '2019-01-10T09:00:00.000Z',
            class: 'EKONOMI',
            benefits: [
                {
                    id: 14,
                    name: 'Train Delay',
                    description: null,
                    compensationDetail: 'IDR 50,000 if delay is greater than 1 hour',
                    detail: null,
                    threshold: 50000,
                    link: null
                },
                {
                    id: 15,
                    name: 'Train Cancellation',
                    description: null,
                    compensationDetail: 'Up to the full price of the ticket',
                    detail: null,
                    threshold: null,
                    link: null
                },
                {
                    id: 16,
                    name: 'Personal Accident',
                    description: null,
                    compensationDetail: 'Up to IDR 60,000,000',
                    detail: null,
                    threshold: 60000000,
                    link: null
                }
            ],
            claims: [

            ],
            coveredUsers: [

            ],
            linkPdf: {
                type: 'policy',

                createdAt: '2019-01-10T11:53:24.000Z'
            },
            agentName: 'Padiciti',
            insuranceName: 'Simas Insurtech',
            type: 'train',
            updatedAt: '2019-01-10T11:53:29.000Z'
        },
        {
            number: 'TR-02-190109-HV25Z',
            bookingCode: 'GHBGV4',
            trainName: 'ARGO PARAHYANGAN',
            trainNumber: '26',
            departureStationCode: 'GMR',
            departureStationName: 'GAMBIR',
            arrivalStationCode: 'BD',
            arrivalStationName: 'BANDUNG',
            scheduledDepartureTime: '2019-01-12T08:30:00.000Z',
            scheduledArrivalTime: '2019-01-12T11:39:00.000Z',
            class: 'EKONOMI',
            benefits: [
                {
                    id: 17,
                    name: 'Train Delay',
                    description: null,
                    compensationDetail: 'IDR 50,000 if delay is greater than 1 hour',
                    detail: null,
                    threshold: 50000,

                    link: null
                }
            ],
            claims: [

            ],
            coveredUsers: [

            ],
            linkPdf: {
                type: 'policy',

                createdAt: '2019-01-09T08:11:36.000Z'
            },
            agentName: 'padiciti.com',
            insuranceName: 'Simas Insurtech',
            type: 'train',
            updatedAt: '2019-01-09T08:11:34.000Z'
        },
        {
            number: 'TR-02-190109-B1ILU',
            bookingCode: 'E31QA2',
            trainName: 'MALABAR',
            trainNumber: '91',
            departureStationCode: 'TA',
            departureStationName: 'TULUNGAGUNG',
            arrivalStationCode: 'KYA',
            arrivalStationName: 'KROYA',
            scheduledDepartureTime: '2019-01-16T11:19:00.000Z',
            scheduledArrivalTime: '2019-01-16T19:14:00.000Z',
            class: 'EKONOMI',
            benefits: [
                {
                    id: 17,
                    name: 'Train Delay',
                    description: null,
                    compensationDetail: 'IDR 50,000 if delay is greater than 1 hour',
                    detail: null,
                    threshold: 50000,

                    link: null
                }
            ],
            claims: [

            ],
            coveredUsers: [

            ],
            linkPdf: {
                type: 'policy',

                createdAt: '2019-01-09T08:11:18.000Z'
            },
            agentName: 'padiciti.com',
            insuranceName: 'Simas Insurtech',
            type: 'train',
            updatedAt: '2019-01-09T08:11:16.000Z'
        },
        {
            number: 'TR-02-190109-F5PE2',
            bookingCode: 'B7YTCL',
            trainName: 'ARGO PARAHYANGAN',
            trainNumber: '22',
            departureStationCode: 'GMR',
            departureStationName: 'GAMBIR',
            arrivalStationCode: 'BD',
            arrivalStationName: 'BANDUNG',
            scheduledDepartureTime: '2019-02-08T01:45:00.000Z',
            scheduledArrivalTime: '2019-02-08T05:01:00.000Z',
            class: 'EKONOMI',
            benefits: [
                {
                    id: 17,
                    name: 'Train Delay',
                    description: null,
                    compensationDetail: 'IDR 50,000 if delay is greater than 1 hour',
                    detail: null,
                    threshold: 50000,

                    link: null
                }
            ],
            claims: [

            ],
            coveredUsers: [

            ],
            linkPdf: {
                type: 'policy',

                createdAt: '2019-01-09T08:54:37.000Z'
            },
            agentName: 'padiciti.com',
            insuranceName: 'Simas Insurtech',
            type: 'train',
            updatedAt: '2019-01-09T08:54:41.000Z'
        },
        {
            number: 'TR-02-190109-MRO9X',
            bookingCode: 'VPBUXK',
            trainName: 'ARGO PARAHYANGAN',
            trainNumber: '33',
            departureStationCode: 'BD',
            departureStationName: 'BANDUNG',
            arrivalStationCode: 'GMR',
            arrivalStationName: 'GAMBIR',
            scheduledDepartureTime: '2019-02-19T01:35:00.000Z',
            scheduledArrivalTime: '2019-02-19T04:50:00.000Z',
            class: 'EKSEKUTIF',
            benefits: [
                {
                    id: 17,
                    name: 'Train Delay',
                    description: null,
                    compensationDetail: 'IDR 50,000 if delay is greater than 1 hour',
                    detail: null,
                    threshold: 50000,

                    link: null
                }
            ],
            claims: [

            ],
            coveredUsers: [

            ],
            linkPdf: {
                type: 'policy',

                createdAt: '2019-01-09T08:54:39.000Z'
            },
            agentName: 'padiciti.com',
            insuranceName: 'Simas Insurtech',
            type: 'train',
            updatedAt: '2019-01-09T08:54:41.000Z'
        }
    ]
};
