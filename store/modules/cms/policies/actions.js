import * as types from '@/store/constants';
import transforms from './transforms';

export default {
    async policyList(context, payload) {
        try {
            const response = await this.$api.call('policy.list', payload);
            const responseData = transforms.list.call(this, response.data);
            context.commit(types.CMS_POLICY_LIST, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_POLICY_LIST, []);
            throw error;
        }
    },

    async policyResend(context, payload) {
        try {
            const response = await this.$api.call('policy.resend', payload);
            const responseData = response.data;
            return responseData;
        } catch (error) {
            throw error;
        }
    }
};
