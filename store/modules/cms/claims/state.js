export default {
    claims: [
        {
            number: 'C-190108-JL0DF',
            rating: 0,
            appealStatus: null,
            policy: {
                number: 'FL-02-181211-OPTVQ',
                agentName: 'Qoala Travel AA',
                insuranceName: 'ACA',
                airline: 'CTV',
                flightNumber: 'CTV916',
                departureAirportCode: 'CGK',
                arrivalAirportCode: 'KNO',
                scheduledDepartureTime: '2018-12-13T12:25:00.000Z',
                scheduledArrivalTime: '2018-12-13T14:45:00.000Z',
                type: 'travel',
                class: null,
                bookingCode: 'KNO015',
                coveredUsers: [
                    {
                        id: 19524,
                        fullName: 'Mamam',
                        email: 'andrideng@gmail.com',
                        phoneNumber: '',
                        birthDate: '1982-07-17',
                        nationality: 'ID',
                        title: 'MALE',
                        cancellationReason: null,
                        relationshipType: null
                    }
                ]
            },
            coveredUsers: [
                {
                    id: 19524,
                    fullName: 'Mamam',
                    email: 'andrideng@gmail.com',
                    phoneNumber: '',
                    birthDate: '1982-07-17',
                    nationality: 'ID',
                    title: 'MALE',
                    cancellationReason: null,
                    relationshipType: null
                }
            ],
            histories: [
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2019-01-08T07:20:46.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamam'
                            }
                        }
                    ],
                    createdAt: '2019-01-08T07:20:44.000Z'
                },
                {
                    status: 'RESUBMIT_DOCUMENT',

                    documents: [

                    ],
                    createdAt: '2019-01-08T07:16:15.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2019-01-08T07:13:41.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamam'
                            }
                        }
                    ],
                    createdAt: '2019-01-08T07:13:39.000Z'
                },
                {
                    status: 'RESUBMIT_DOCUMENT',

                    documents: [

                    ],
                    createdAt: '2019-01-08T07:02:05.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2019-01-08T06:51:28.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamam'
                            }
                        }
                    ],
                    createdAt: '2019-01-08T06:51:27.000Z'
                },
                {
                    status: 'RESUBMIT_DOCUMENT',

                    documents: [

                    ],
                    createdAt: '2019-01-08T06:51:00.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2019-01-08T06:49:39.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamam'
                            }
                        }
                    ],
                    createdAt: '2019-01-08T06:49:37.000Z'
                }
            ],
            claimer: {

            },
            benefit: {
                id: 2,
                name: 'Flight Delay',

                detail: [
                    {
                        max: 4,
                        min: 2,
                        value: 150000
                    },
                    {
                        max: 8,
                        min: 4,
                        value: 500000
                    },
                    {
                        max: 12,
                        min: 8,
                        value: 1000000
                    },
                    {
                        max: 16,
                        min: 12,
                        value: 1500000
                    },
                    {
                        max: 20,
                        min: 16,
                        value: 2000000
                    },
                    {
                        max: 24,
                        min: 20,
                        value: 2500000
                    },
                    {
                        max: 30,
                        min: 24,
                        value: 3000000
                    }
                ],
                threshold: 3000000

            },
            updatedAt: '2019-01-08T07:20:44.000Z'
        },
        {
            number: 'C-181126-FC5XO',
            rating: 0,
            appealStatus: null,
            policy: {
                number: 'FL-10-181126-PC4FO',
                agentName: 'Ando Travel',
                insuranceName: 'ACA',
                airline: 'GA',
                flightNumber: 'GA216',
                departureAirportCode: 'CGK',
                arrivalAirportCode: 'JOG',
                scheduledDepartureTime: '2018-11-25T12:25:00.000Z',
                scheduledArrivalTime: '2018-11-25T13:40:00.000Z',
                type: 'travel',
                class: null,
                bookingCode: 'JOG095',
                coveredUsers: [
                    {
                        id: 19205,
                        fullName: 'Mamat',
                        email: 'deng.andri@gmail.com',
                        phoneNumber: null,
                        birthDate: null,
                        nationality: 'ID',
                        title: null,
                        cancellationReason: null,
                        relationshipType: null
                    }
                ]
            },
            coveredUsers: [
                {
                    id: 19205,
                    fullName: 'Mamat',
                    email: 'deng.andri@gmail.com',
                    phoneNumber: null,
                    birthDate: null,
                    nationality: 'ID',
                    title: null,
                    cancellationReason: null,
                    relationshipType: null
                }
            ],
            histories: [
                {
                    status: 'PAID',

                    documents: [

                    ],
                    createdAt: '2018-12-21T07:24:33.000Z'
                },
                {
                    status: 'APPROVED',

                    documents: [

                    ],
                    createdAt: '2018-11-26T06:48:20.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-11-26T06:47:22.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamat'
                            }
                        }
                    ],
                    createdAt: '2018-11-26T06:47:20.000Z'
                }
            ],
            claimer: {
                id: 3,
                fullName: 'martin',
                email: 'martin@qoala.id',
                phone: '+6281280637766'
            },
            benefit: {
                id: 24,
                name: 'Flight Delay',

                detail: [
                    {
                        max: 4,
                        min: 1.5,
                        value: 150000
                    },
                    {
                        max: 8,
                        min: 4,
                        value: 500000
                    },
                    {
                        max: 12,
                        min: 8,
                        value: 1000000
                    },
                    {
                        max: 16,
                        min: 12,
                        value: 1500000
                    },
                    {
                        max: 20,
                        min: 16,
                        value: 2000000
                    },
                    {
                        max: 24,
                        min: 20,
                        value: 2500000
                    },
                    {
                        max: 30,
                        min: 24,
                        value: 3000000
                    }
                ],
                threshold: 3000000

            },
            updatedAt: '2018-12-21T07:24:37.000Z'
        },
        {
            number: 'C-181007-WKQPH',
            rating: 0,
            appealStatus: null,
            policy: {
                number: 'FL-06-181005-PQXMF',
                agentName: 'Padiciti',
                insuranceName: 'Simas Insurtech',
                trainNumber: 'JYB-002',
                trainName: 'Jayabaya',
                departureStationCode: 'GMR',
                departureStationName: 'GAMBIR',
                arrivalStationCode: 'ML',
                arrivalStationName: 'MALANG',
                scheduledDepartureTime: '2018-10-05T07:00:00.000Z',
                scheduledArrivalTime: '2018-10-05T09:30:00.000Z',
                type: 'train',
                class: 'EKONOMI',
                bookingCode: 'TRN125',
                coveredUsers: [
                    {
                        id: 244,
                        fullName: 'Erando Putra',
                        email: 'erando.putra@qoala.id',
                        phoneNumber: null,
                        birthDate: null,
                        nationality: null,
                        title: null,
                        cancellationReason: null,
                        relationshipType: null
                    },
                    {
                        id: 245,
                        fullName: 'Martin',
                        email: 'martin@qoala.id',
                        phoneNumber: null,
                        birthDate: null,
                        nationality: null,
                        title: null,
                        cancellationReason: null,
                        relationshipType: null
                    }
                ]
            },
            coveredUsers: [
                {
                    id: 244,
                    fullName: 'Erando Putra',
                    email: 'erando.putra@qoala.id',
                    phoneNumber: null,
                    birthDate: null,
                    nationality: null,
                    title: null,
                    cancellationReason: null,
                    relationshipType: null
                },
                {
                    id: 245,
                    fullName: 'Martin',
                    email: 'martin@qoala.id',
                    phoneNumber: null,
                    birthDate: null,
                    nationality: null,
                    title: null,
                    cancellationReason: null,
                    relationshipType: null
                }
            ],
            histories: [
                {
                    status: 'PAID',

                    documents: [

                    ],
                    createdAt: '2018-12-21T07:23:02.000Z'
                },
                {
                    status: 'APPROVED',

                    documents: [

                    ],
                    createdAt: '2018-10-07T00:58:29.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-07T00:54:46.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        },
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-07T00:54:47.000Z',
                            ownerCoveredUser: {
                                fullName: 'Martin'
                            }
                        }
                    ],
                    createdAt: '2018-10-07T00:54:45.000Z'
                }
            ],
            claimer: {
                id: 3,
                fullName: 'martin',
                email: 'martin@qoala.id',
                phone: '+6281280637766'
            },
            benefit: {
                id: 16,
                name: 'Personal Accident',

                detail: null,
                threshold: 60000000

            },
            updatedAt: '2018-12-21T07:23:04.000Z'
        },
        {
            number: 'C-181005-Q3H8N',
            rating: 0,
            appealStatus: null,
            policy: {
                number: 'FL-06-181005-PQXMF',
                agentName: 'Padiciti',
                insuranceName: 'Simas Insurtech',
                trainNumber: 'JYB-002',
                trainName: 'Jayabaya',
                departureStationCode: 'GMR',
                departureStationName: 'GAMBIR',
                arrivalStationCode: 'ML',
                arrivalStationName: 'MALANG',
                scheduledDepartureTime: '2018-10-05T07:00:00.000Z',
                scheduledArrivalTime: '2018-10-05T09:30:00.000Z',
                type: 'train',
                class: 'EKONOMI',
                bookingCode: 'TRN125',
                coveredUsers: [
                    {
                        id: 244,
                        fullName: 'Erando Putra',
                        email: 'erando.putra@qoala.id',
                        phoneNumber: null,
                        birthDate: null,
                        nationality: null,
                        title: null,
                        cancellationReason: null,
                        relationshipType: null
                    }
                ]
            },
            coveredUsers: [
                {
                    id: 244,
                    fullName: 'Erando Putra',
                    email: 'erando.putra@qoala.id',
                    phoneNumber: null,
                    birthDate: null,
                    nationality: null,
                    title: null,
                    cancellationReason: null,
                    relationshipType: null
                }
            ],
            histories: [
                {
                    status: 'PAID',

                    documents: [

                    ],
                    createdAt: '2018-12-21T07:19:59.000Z'
                },
                {
                    status: 'APPROVED',

                    documents: [

                    ],
                    createdAt: '2018-10-05T09:00:40.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T08:57:59.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        },
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T08:57:59.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        }
                    ],
                    createdAt: '2018-10-05T08:57:57.000Z'
                },
                {
                    status: 'RESUBMIT_DOCUMENT',

                    documents: [

                    ],
                    createdAt: '2018-10-05T07:44:38.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T07:42:54.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        },
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T07:42:54.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        }
                    ],
                    createdAt: '2018-10-05T07:42:52.000Z'
                },
                {
                    status: 'RESUBMIT_DOCUMENT',

                    documents: [

                    ],
                    createdAt: '2018-10-05T07:20:27.000Z'
                },
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T07:09:53.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        },
                        {
                            type: 'claim-document',

                            createdAt: '2018-10-05T07:09:53.000Z',
                            ownerCoveredUser: {
                                fullName: 'Erando Putra'
                            }
                        }
                    ],
                    createdAt: '2018-10-05T07:09:51.000Z'
                }
            ],
            claimer: {
                id: 3,
                fullName: 'martin',
                email: 'martin@qoala.id',
                phone: '+6281280637766'
            },
            benefit: {
                id: 14,
                name: 'Train Delay',

                detail: null,
                threshold: 50000

            },
            updatedAt: '2018-12-21T07:20:02.000Z'
        },
        {
            number: 'C-181217-SSAMG',
            rating: 0,
            appealStatus: null,
            policy: {
                number: 'FL-02-181128-MHZQ8',
                agentName: 'Ando Travel',
                insuranceName: 'ACA',
                airline: 'GA',
                flightNumber: 'GA216',
                departureAirportCode: 'JOG',
                arrivalAirportCode: 'CGK',
                scheduledDepartureTime: '2018-12-06T12:25:00.000Z',
                scheduledArrivalTime: '2018-12-06T13:40:00.000Z',
                type: 'travel',
                class: null,
                bookingCode: 'JOG115',
                coveredUsers: [
                    {
                        id: 19219,
                        fullName: 'Mamat',
                        email: 'deng.andri@gmail.com',
                        phoneNumber: null,
                        birthDate: null,
                        nationality: 'ID',
                        title: null,
                        cancellationReason: null,
                        relationshipType: null
                    }
                ]
            },
            coveredUsers: [
                {
                    id: 19219,
                    fullName: 'Mamat',
                    email: 'deng.andri@gmail.com',
                    phoneNumber: null,
                    birthDate: null,
                    nationality: 'ID',
                    title: null,
                    cancellationReason: null,
                    relationshipType: null
                }
            ],
            histories: [
                {
                    status: 'INITIATED',

                    documents: [
                        {
                            type: 'claim-document',

                            createdAt: '2018-12-17T08:05:42.000Z',
                            ownerCoveredUser: {
                                fullName: 'Mamat'
                            }
                        }
                    ],
                    createdAt: '2018-12-17T08:05:41.000Z'
                }
            ],
            claimer: {

            },
            benefit: {
                id: 2,
                name: 'Flight Delay',

                detail: [
                    {
                        max: 4,
                        min: 2,
                        value: 150000
                    },
                    {
                        max: 8,
                        min: 4,
                        value: 500000
                    },
                    {
                        max: 12,
                        min: 8,
                        value: 1000000
                    },
                    {
                        max: 16,
                        min: 12,
                        value: 1500000
                    },
                    {
                        max: 20,
                        min: 16,
                        value: 2000000
                    },
                    {
                        max: 24,
                        min: 20,
                        value: 2500000
                    },
                    {
                        max: 30,
                        min: 24,
                        value: 3000000
                    }
                ],
                threshold: 3000000
            },
            updatedAt: '2018-12-17T08:05:41.000Z'
        }
    ]
};
