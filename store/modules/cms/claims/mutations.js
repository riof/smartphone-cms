import * as types from '@/store/constants';

export default {
    // claimList
    [types.CMS_CLAIM_LIST](state, value) {
        state.claims = value;
    },
    // claimListByPolicy
    [types.CMS_CLAIM_LIST_BY_POLICY](state, value) {
        state.claimsByPolicy = value;
    },
    // claimUpdate
    [types.CMS_CLAIM_UPDATE](state, value) {
        state.claim = value;
    }
};
