import * as types from '@/store/constants';
import transforms from './transforms';

export default {
    async claimList(context, payload = {}) {
        try {
            const response = await this.$api.call('claim.list', payload);
            const responseData = transforms.list.call(this, response.data);
            context.commit(types.CMS_CLAIM_LIST, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_CLAIM_LIST, []);
            throw error;
        }
    },

    async claimListByPolicy(context, payload) {
        try {
            const response = await this.$api.call('claim.listByPolicy', payload);
            const responseData = transforms.listByPolicy.call(this, response.data);
            context.commit(types.CMS_CLAIM_LIST_BY_POLICY, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_CLAIM_LIST_BY_POLICY, []);
            throw error;
        }
    },

    async documentSingle(context, payload) {
        try {
            const { params, data } = payload;
            if (!params.documentId) return '';
            const response = await this.$api.call('document.single', { params });
            const dotSplit = params.documentId.split('.');
            const imgType = dotSplit[dotSplit.length - 1];
            const base64 = `data:image/${imgType};base64,${response}`;
            if (data) {
                context.commit(types.CMS_USER_DOCUMENT_GET, { docType: data.docType, base64 });
            }
            return base64;
        } catch (error) {
            return '';
        }
    },

    async claimUpdate(context, payload) {
        try {
            const response = await this.$api.call('claim.update', payload);
            const responseData = response.data;
            context.commit(types.CMS_CLAIM_UPDATE, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_CLAIM_UPDATE, {});
            throw error;
        }
    }
};
