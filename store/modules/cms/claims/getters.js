export default {
    getClaim: state => state.claim,
    getClaims: state => state.claims,
    getClaimsByPolicy: state => state.claimsByPolicy
};
