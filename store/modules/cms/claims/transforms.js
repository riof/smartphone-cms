export default {
    list(data) {
        if (!data.length) return [];
        return data;
    },

    listByPolicy(data) {
        if (!data.length) return [];

        return data.map(claim => {
            claim.payment_status = '';
            claim.payment_created_at = '';
            claim.payment_updated_at = '';

            if (claim.amounts !== undefined && claim.amounts.length) {
                claim.payment_status = claim.amounts[0].status;
                claim.payment_created_at = claim.amounts[0].created_at;
                claim.payment_updated_at = claim.amounts[0].updated_at;
            }

            return claim;
        });
    }
};
