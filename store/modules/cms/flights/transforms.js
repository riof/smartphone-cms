export default {
    list(data) {
        if (!data.length) return [];

        const generateStatus = status => this.$flightHelpers.mapFlightStatus(status);

        const generateStatusClass = (status) => {
            if (['SO', 'EO', 'LO'].indexOf(status) >= 0) return 'green';
            if (['SD', 'ED', 'LD'].indexOf(status) >= 0) return 'red';
            if (status === 'CL') return 'yellow';
            if (status === 'UK') return 'blue-grey';
            return '';
        };

        const defineChildAutoManual = (claims, type) => {
            let total = 0;
            let initiated = 0;
            if (claims.length > 0) {
                for (const claim of claims) {
                    if (claim.type === type) {
                        // const { status } = claim.histories[claim.histories.length - 1];
                        const status = claim.lastStatus;
                        if (status === 'INITIATED') initiated++;
                        total++;
                    }
                }
            }
            return { initiated, total };
        };

        const defineAutoManual = (policies, type) => {
            let total = 0;
            let initiated = 0;
            if (policies.length > 0) {
                for (const policy of policies) {
                    ({ total, initiated } = defineChildAutoManual(policy.claims, type));
                }
            }
            return { initiated, total };
        };

        return data.map(
            v => {
                const status = v.statuses.length > 0 ? v.statuses[v.statuses.length - 1].code : 0;
                const manualClaim = defineAutoManual(v.policies, 'MANUAL');
                const autoClaim = defineAutoManual(v.policies, 'AUTO');
                const pendingClaim = manualClaim.initiated;

                const policies = v.policies.length === 0 ? [] : v.policies.map(policy => {
                    policy.manualClaim = defineChildAutoManual(policy.claims, 'MANUAL');
                    policy.autoClaim = defineChildAutoManual(policy.claims, 'AUTO');
                    policy.pendingClaim = policy.manualClaim.initiated;
                    return policy;
                });

                return Object.assign(v, {
                    status,
                    statusText: generateStatus(status),
                    statusClass: generateStatusClass(status),
                    totalPolicies: v.policies.length,
                    manualClaim: manualClaim.total,
                    autoClaim: autoClaim.total,
                    pendingClaim,
                    policies
                });
            }
        );
    }
};
