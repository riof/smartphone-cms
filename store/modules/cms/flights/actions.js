import * as types from '@/store/constants';
import transforms from './transforms';

export default {
    async flightList(context, payload) {
        try {
            const response = await this.$api.call('flight.list', payload);
            const responseData = transforms.list.call(this, response.data);
            context.commit(types.CMS_FLIGHT_LIST, responseData);
            return response;
        } catch (error) {
            context.commit(types.CMS_FLIGHT_LIST, []);
            throw error;
        }
    },

    // - get backlog
    async flightBacklogList(context, payload) {
        try {
            const response = await this.$api.call('flight.backlogList', payload);
            const responseData = transforms.list.call(this, response.data);
            context.commit(types.CMS_FLIGHT_BACKLOG_LIST, responseData);
            return response;
        } catch (error) {
            console.log(error);
            context.commit(types.CMS_FLIGHT_BACKLOG_LIST, []);
            throw error;
        }
    },

    async flightUpdateDelay(context, payload) {
        try {
            const response = await this.$api.call('flight.updateDelay', payload);
            const responseData = response.data;
            return responseData;
        } catch (error) {
            throw error;
        }
    },

    async flightPolicySingle(context, payload) {
        try {
            const response = await this.$api.call('flight.policy', payload);
            const responseData = response.data;
            context.commit(types.CMS_FLIGHT_POLICY_SINGLE, responseData);
            return responseData;
        } catch (error) {
            context.commit(types.CMS_FLIGHT_POLICY_SINGLE, {});
            throw error;
        }
    },

    async flightStatusCreate(context, payload) {
        try {
            const response = await this.$api.call('flight.statusCreate', payload);
            const responseData = response.data;
            return responseData;
        } catch (error) {
            throw error;
        }
    },

    async flightForceStatusCreate(context, payload) {
        try {
            const response = await this.$api.call('flight.forceStatusCreate', payload);
            const responseData = response.data;
            return responseData;
        } catch (error) {
            throw error;
        }
    },

    async flightClaimCreate(context, payload) {
        try {
            const response = await this.$api.call('flight.claimCreate', payload);
            const responseData = response.data;
            context.commit(types.CMS_FLIGHT_CLAIM_CREATE, responseData);
            return responseData;
        } catch (error) {
            throw error;
        }
    }
};
