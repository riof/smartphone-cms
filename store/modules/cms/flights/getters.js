export default {
    getFlights: state => state.flights,
    getBacklogFlights: state => state.flights,
    getFlightPolicy: state => state.policy
};
