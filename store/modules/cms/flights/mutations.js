import * as types from '@/store/constants';

export default {
    // flightList
    [types.CMS_FLIGHT_LIST](state, value) {
        state.flights = value;
    },

    // flightBacklogList
    [types.CMS_FLIGHT_BACKLOG_LIST](state, value) {
        state.flights = value;
    },

    // flightPolicySingle
    [types.CMS_FLIGHT_POLICY_SINGLE](state, value) {
        state.policy = value;
    },

    // flightClaimCreate
    [types.CMS_FLIGHT_CLAIM_CREATE]() {
        // state.flights = value;
    }
};
