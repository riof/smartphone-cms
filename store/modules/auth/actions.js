import * as types from '@/store/constants';

export default {
    async authCreate(context, payload) {
        try {
            const response = await this.$api.call('auth.create', payload);
            const responseData = response.data;
            context.commit(types.AUTH_CREATE, responseData);
            context.dispatch('intervalToken', {
                data: {
                    refresh: responseData.refresh
                }
            });
            return responseData;
        } catch (error) {
            context.commit(types.AUTH_CREATE, {});
            context.dispatch('authDestroy');
            throw error;
        }
    },

    async authRefresh(context, payload) {
        try {
            // payload.opt = { progress: false };
            const response = await this.$api.call('auth.refresh', payload);
            const responseData = response.data;
            context.commit(types.AUTH_CREATE, responseData);
            context.dispatch('intervalToken', {
                data: {
                    refresh: responseData.refresh
                }
            });
            return responseData;
        } catch (error) {
            context.commit(types.AUTH_CREATE, {});
            context.dispatch('authDestroy');
            throw error;
        }
    },

    intervalToken(context, payload) {
        const TIMEOUT = 25 * 60 * 1000; // 25 mins for 30 mins ttl token
        clearTimeout(context.state.callRefreshToken);
        context.state.callRefreshToken = setTimeout(() => {
            context.dispatch('authRefresh', payload);
        }, TIMEOUT);
    },

    async authDestroy(context) {
        try {
            await this.$api.call('auth.destroy', {
                params: {
                    token: context.getters.getUserToken
                }
            });
            context.commit(types.AUTH_DESTROY);
            clearTimeout(context.state.callRefreshToken);
            this.$router.push('/');
        } catch (error) {
            throw error;
        }
    }
};
