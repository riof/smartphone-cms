import * as types from '@/store/constants';

export default {
    [types.TOGGLE_APP_DRAWER](state, payload) {
        state.drawer = payload.drawer;
    },

    [types.TOGGLE_APP_SNACKBAR](state, payload) {
        state.snackbar = payload;
    }
};
