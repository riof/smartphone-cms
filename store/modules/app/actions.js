import * as types from '@/store/constants';

export default {
    // - toggle side menu drawer
    toggleDrawer(context) {
        context.commit(types.TOGGLE_APP_DRAWER, {
            drawer: !context.state.drawer
        });
    },

    // - toggle snackbar with message
    toggleSnackBar(context, payload) {
        context.commit(types.TOGGLE_APP_SNACKBAR, payload);
    }
};
