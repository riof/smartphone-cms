export default function (context) {
    if (context.route.name === 'train-policies-policyNumber') {
        const trailing = context.route.fullPath.endsWith('/') ? '' : '/';
        context.app.router.push(`${context.route.fullPath}${trailing}delay`);
    }
    if (context.route.name === 'train-train') {
        context.app.router.push('/train/delay');
    }
}
