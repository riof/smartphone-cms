import Cookies from 'js-cookie';

export default function ({ store }) {
    store.$api.call('auth.check', {
        data: {
            token: Cookies.get('t')
        }
    }).catch(() => {
        store.dispatch('authDestroy');
    });
}
