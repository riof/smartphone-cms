// title, name, icon, disabled, href, query, target, group
const Menu = [{
        name: 'Home',
        link: '/home',
        icon: 'home'
    },
    {
        name: 'List',
        link: '/list',
        icon: 'list'
    },
    {
        name: 'Employee',
        link: '/employee',
        icon: 'error_outline'
    },
    {
        name: 'Users',
        link: '/user',
        icon: 'error_outline'
    },
];

export default Menu;
