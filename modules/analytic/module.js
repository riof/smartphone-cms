const path = require('path');

const defaults = {
    debug: false
};

module.exports = function mixpanelModule(moduleOptions) {
    const options = Object.assign({}, defaults, moduleOptions);

    // Register plugin
    this.addPlugin({
        src: path.resolve(__dirname, 'plugin.js'),
        options
    });
};
