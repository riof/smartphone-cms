/* eslint-disable */

const mixpanel = require('mixpanel-browser');

const mixpanelPlugin = (config) => {
    if (!mixpanel && !config.token) return {};

    mixpanel.init(config.token);

    const logDebug = (...args) => (config.debug ? console.log('MixPanel: ', args) : '');

    return {
        trackView(viewName) {
            if (!mixpanel.track) return;
            logDebug('trackView', viewName);

            mixpanel.track('pageview: ' + viewName);
        },

        trackEvent({ action, properties = {}, callback = null }) {
            if (!mixpanel.track) return;
            logDebug(...arguments);

            const mixpanelProperties = Object.assign({}, properties);
            mixpanel.track(action, mixpanelProperties, callback);
        },

        setAlias(alias) {
            if (!mixpanel.alias) return;
            logDebug(alias);

            mixpanel.alias(alias);
        },

        identify({ userId }) {
            if (!mixpanel.identify) return;
            if (!userId) return;
            logDebug(userId);

            mixpanel.identify(userId);
        },

        setUsername(userId) {
            if (!mixpanel.identity) return;
            logDebug(userId);

            mixpanel.identify(userId);
        },

        setUserProperties(properties = {}) {
            if (!mixpanel.people) return;
            logDebug(properties);

            mixpanel.people.set(properties);
        },

        setUserPropertiesOnce(properties) {
            if (!mixpanel.people) return;
            logDebug(properties);

            mixpanel.people.set_once(properties);
        },

        incrementUserProperties(properties) {
            if (!mixpanel.people) return;
            logDebug(properties);

            mixpanel.people.increment(properties);
        },

        setSuperProperties(properties) {
            if (!mixpanel.register) return;
            logDebug(properties);

            mixpanel.register(properties);
        },

        setSuperPropertiesOnce(properties) {
            if (!mixpanel.register_once) return;
            logDebug(properties);

            mixpanel.register_once(properties);
        },

        reset() {
            if (!mixpanel.reset) return;
            logDebug('reset');

            mixpanel.reset();
        }
    };
};

export default ({ app: { router, store } }, inject) => {
    inject('analytic', mixpanelPlugin({
        token: '<%= options.token %>',
        debug: '<%= options.debug %>'
    }));

    router.afterEach((to, from) => {
        //
    });
};
