FROM node:10-alpine

WORKDIR /var/app

COPY package*.json ./

RUN ["npm", "install", "--only=production"]
COPY . .
RUN ["npm", "run", "build"]


EXPOSE 9003

CMD [ "npm", "run", "start" ]