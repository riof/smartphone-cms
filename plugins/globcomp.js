// global components

import Vue from 'vue';
import CConfirm from '@/components/Controls/CConfirm.vue';
import CUpload from '@/components/Controls/CUpload.vue';

Vue.component('c-confirm', CConfirm);
Vue.component('c-upload', CUpload);
