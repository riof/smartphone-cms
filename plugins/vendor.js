import lodash from 'lodash';
import moment from 'moment';
import viewer from 'viewerjs';

export default (context, inject) => {
    inject('_', lodash);
    inject('moment', moment);
    inject('viewer', viewer);
};
