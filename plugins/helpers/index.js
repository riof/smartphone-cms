import helpers from './common';
import flightHelpers from './flight';
import trainHelpers from './train';
import shipperHelpers from './shipper';

export default ({ app }, inject) => {
    inject('helpers', helpers(app));
    inject('flightHelpers', flightHelpers(app));
    inject('trainHelpers', trainHelpers(app));
    inject('shipperHelpers', shipperHelpers(app));
};
