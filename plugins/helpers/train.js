export default () => ({
    // map policy type
    mapBenefitCode(type) {
        const benefitCode = {
            delay: 'TD',
            general: 'NOT_TD'
        };

        return benefitCode[type];
    },

    // map policy type
    mapPageType(type) {
        const pageType = {
            'train-delay': 'delay',
            'train-general': 'general',
            'train-policies-policyNumber-delay': 'delay',
            'train-policies-policyNumber-general': 'general'
        };

        return pageType[type];
    },

    // map train status
    mapTrainStatus(status) {
        const trainStatus = {
            SO: 'Scheduled On Time',
            SD: 'Scheduled Delay',
            AO: 'Arrived On Time',
            AD: 'Arrived Delay',
            EO: 'En Route On Time',
            ED: 'En Route Delay',
            CL: 'Cancelled',
            UK: 'Unknown'
        };

        return trainStatus[status] || 'On Process';
    },


    trainInformation(item) {
        return `
        <span>${item.name} | ${item.number}</span><br/>
        <span>(${item.departureStation.code} -> ${item.arrivalStation.code})</span><br/>
      `;
    }
});
