export default () => ({
    // map claim status
    mapClaimState(state) {
        const availableState = {
            INITIATED: 'User Data Submitted',
            RESUBMIT_DOCUMENT: 'Resubmit Document',
            REJECTED: 'Claim Rejected',
            SOFT_REJECT: 'Claim already process by others',
            APPROVED: 'Claim Approved',
            PRE_APPROVED: 'Waiting for User Information Verification',
            PAID: 'Claim Paid'
        };

        if (!state) return 'Unknown';

        return availableState[state];
    },
    // get error data
    getErrorData(e) {
        if (e.response.data && e.response.data.message) {
            return e.response.data;
        }
        return e;
    }
});
