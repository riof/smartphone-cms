export default app => ({
    // map policy type
    mapBenefitCode(type) {
        const benefitCode = {
            delay: 'FD',
            general: 'NOT_FD'
        };

        return benefitCode[type];
    },

    // map policy type
    mapPageType(type) {
        const pageType = {
            'flight-delay': 'delay',
            'flight-general': 'general',
            'flight-policies-policyNumber-delay': 'delay',
            'flight-policies-policyNumber-general': 'general'
        };

        return pageType[type];
    },

    // map flight status
    mapFlightStatus(status) {
        const flightStatus = {
            SO: 'Scheduled On Time',
            SD: 'Scheduled Delay',
            LO: 'Landed On Time',
            LD: 'Landed Delay',
            EO: 'En Route On Time',
            ED: 'En Route Delay',
            CL: 'Cancelled',
            UK: 'Unknown'
        };

        return flightStatus[status] || 'On Process';
    },

    // define option flight
    defineFlightOption(code, aircraft, number) {
        const fNumber = number.split(aircraft)[1];
        if (['JT', 'ID', 'XT', 'IN'].indexOf(aircraft) > -1) {
            aircraft += '*';
        }
        const flightradar = `https://www.flightradar24.com/data/flights/${number}`;
        const flightstats = `https://www.flightstats.com/v2/flight-tracker/${aircraft}/${fNumber}`;
        const flightaware = `https://flightaware.com/live/flight/${number}`;
        const flightgoogle = `https://google.com/search?q=${number}`;

        const options = {
            FS: flightstats,
            FA: flightaware,
            FR: flightradar,
            GO: flightgoogle
        };

        return options[code];
    },

    // flight link
    flightLink(aircraft, number) {
        return (
            '<span>'
          + `<a href="${this.defineFlightOption('FA', aircraft, number)}" target="_blank">FA</a> - `
          + `<a href="${this.defineFlightOption('FS', aircraft, number)}" target="_blank">FS</a> - `
          + `<a href="${this.defineFlightOption('FR', aircraft, number)}" target="_blank">FR</a> - `
          + `<a href="${this.defineFlightOption('GO', aircraft, number)}" target="_blank">GO</a>`
        + '</span>'
        );
    },

    flightInformation(item) {
        return `
        <span>${item.number} (${item.departureAirport.code} -> ${item.arrivalAirport.code})</span><br/>
        <span>${this.flightLink(item.aircraft, item.number)}</span><br/>
      `;
    },

    // date format
    dateFormat(date) {
        return app.$moment(date).format('ddd, DD - MMMM - YYYY, HH:MM');
    }
});
