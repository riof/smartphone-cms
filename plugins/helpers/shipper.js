export default () => ({
    // map policy type
    mapBenefitCode(type) {
        const benefitCode = {
            delay: 'TD',
            general: 'NOT_TD'
        };

        return benefitCode[type];
    },

    // map policy type
    mapPageType(type) {
        const pageType = {
            'shipper-delay': 'delay',
            'shipper-general': 'general',
            'shipper-policies-policyNumber-delay': 'delay',
            'shipper-policies-policyNumber-general': 'general'
        };

        return pageType[type];
    },

    // map shipper status
    mapShipperStatus(status) {
        const trainStatus = {
            SO: 'Scheduled On Time',
            SD: 'Scheduled Delay',
            AO: 'Arrived On Time',
            AD: 'Arrived Delay',
            EO: 'En Route On Time',
            ED: 'En Route Delay',
            CL: 'Cancelled',
            UK: 'Unknown'
        };

        return trainStatus[status] || 'On Process';
    },


    shipperInformation(item) {
        return `
        <span>${item.name} | ${item.number}</span><br/>
        <span>(${item.departureStation.code} -> ${item.arrivalStation.code})</span><br/>
      `;
    }
});
