export default {
    auth: {
        create: { method: 'post', url: '/sessions' },
        refresh: { method: 'post', url: '/sessions/refresh' },
        check: { method: 'post', url: '/sessions/check' },
        destroy: { method: 'delete', url: '/sessions/:token' }
    },
    document: {
        single: { method: 'get', url: '/documents/:documentId' }
    },
    user: {
        list: { method: 'get', url: '/cms/users/list' },
        single: { method: 'get', url: '/cms/users/get/:userId' },
        updateStatus: { method: 'put', url: '/cms/users/update-status' }
    },
    flight: {
        list: { method: 'post', url: '/cms/flights/list' },
        backlogList: { method: 'get', url: '/cms/flights/backlog-list' },
        updateDelay: { method: 'put', url: '/cms/flights/update-delay-amount' },
        policy: { method: 'get', url: '/cms/flight-policy/:number' },
        statusCreate: { method: 'post', url: '/cms/flights/create-status' },
        forceStatusCreate: { method: 'post', url: '/cms/flights/force-create-status' },
        claimCreate: { method: 'post', url: '/cms/claim-history/create' }
    },
    train: {
        list: { method: 'post', url: '/cms/trains/list' },
        updateDelay: { method: 'put', url: '/cms/trains/update-delay-amount' },
        policy: { method: 'get', url: '/cms/train-policy/:number' },
        claimCreate: { method: 'post', url: '/cms/train-claim/create' }
    },
    shipper: {
        policyList: { method: 'get', url: '/cms/shipper-policy/list' },
        policy: { method: 'get', url: '/cms/shipper-policy/:number' },
        claimList: { method: 'get', url: '/cms/shipper-claim/list' },
        claim: { method: 'get', url: '/cms/shipper-claim/:number' },
        claimCreate: { method: 'post', url: '/cms/shipper-claim/claim-history/create' }
    },
    claim: {
        list: { method: 'get', url: '/cms/claims/list' },
        listByPolicy: { method: 'get', url: '/cms/claims/get/:policyId/:benefitCode' },
        update: { method: 'put', url: '/cms/claims/update' }
    },
    policy: {
        list: { method: 'get', url: '/cms/policies/list' },
        resend: { method: 'post', url: '/cms/policies/resend-policy' }
    },
    payment: {
        list: { method: 'get', url: '/cms/payment/list' },
        update: { method: 'put', url: '/cms/payment/update' },
        confirm: { method: 'put', url: '/cms/payment/confirm' }
    },
    agent: {
        list: { method: 'get', url: '/cms/agents/list' },
        single: { method: 'get', url: '/cms/agents/get/:id' },
        create: { method: 'post', url: '/cms/agents/create' },
        update: { method: 'put', url: '/cms/agents/update' },
        destroy: { method: 'delete', url: '/cms/agents/destroy' }
    },
    partner: {
        list: { method: 'get', url: '/cms/partners/list' },
        single: { method: 'get', url: '/cms/partners/get/:id' },
        create: { method: 'post', url: '/cms/partners/create' },
        update: { method: 'put', url: '/cms/partners/update' },
        destroy: { method: 'delete', url: '/cms/partners/destroy' }
    },
    insurancePartner: {
        list: { method: 'get', url: '/cms/insurance-partners/list' },
        single: { method: 'get', url: '/cms/insurance-partners/get/:id' },
        create: { method: 'post', url: '/cms/insurance-partners/create' },
        update: { method: 'put', url: '/cms/insurance-partners/update' },
        destroy: { method: 'delete', url: '/cms/insurance-partners/destroy' }
    },
    product: {
        list: { method: 'get', url: '/cms/products/list' },
        single: { method: 'get', url: '/cms/products/get/:id' },
        create: { method: 'post', url: '/cms/products/create' },
        update: { method: 'put', url: '/cms/products/update' },
        destroy: { method: 'delete', url: '/cms/products/destroy' }
    },
    benefit: {
        list: { method: 'get', url: '/cms/benefits/list' },
        single: { method: 'get', url: '/cms/benefits/get/:id' },
        create: { method: 'post', url: '/cms/benefits/create' },
        update: { method: 'put', url: '/cms/benefits/update' },
        destroy: { method: 'delete', url: '/cms/benefits/destroy' }
    },
    dashboard: {
        generateReport: { method: 'get', url: '/cms/report/generate-csv' },
        sendEmail: { method: 'post', url: '/cms/cs/send-email' }
    }
};
