export default {
    // list all trains
    async list(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // flight backlog
    async backlogList(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // update delay amount
    async updateDelay(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // get single policy
    async policy(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // create status
    async statusCreate(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // force create status
    async forceStatusCreate(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // create claim
    async claimCreate(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};
