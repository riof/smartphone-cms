export default {
    // list all claims
    async list(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // list all claims by policy
    async listByPolicy(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // update single claim
    async update(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};
