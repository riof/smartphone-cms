export default {
    // list all user
    async list(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // get single user
    async single(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // update status
    async updateStatus(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};
