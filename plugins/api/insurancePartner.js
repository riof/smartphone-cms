export default {
    // list all insurance partner
    async list(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // single insurance partner
    async single(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // create insurance partner
    async create(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // update insurance partner
    async update(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // destroy insurance partner
    async destroy(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};
