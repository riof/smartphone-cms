export default {
    // list all agent
    async list(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // single agent
    async single(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // create agent
    async create(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // update agent
    async update(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    // destroy agent
    async destroy(context) {
        try {
            const response = await this.$axios(context);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};
