import Vue from 'vue';
import Vuetify from 'vuetify';
import VDateRange from 'vuetify-daterange-picker';

Vue.use(Vuetify);
Vue.use(VDateRange);
