import Vue from 'vue';
import formatCurrency from 'format-currency';

export default ({ app }) => {
    const filters = {
        valueFilter(value) {
            if (app.$_.isNil(value)) return '-';
            return value;
        },

        dateFilter(value) {
            if (app.$_.isEmpty(value)) return '-';
            return app.$moment(value).format('ddd, DD-MMMM-YYYY, HH:mm');
        },

        datePictureFilter(value, diff = 0) {
            if (app.$_.isEmpty(value)) return '-';
            return app.$moment(value).subtract(diff, 'seconds')
                .format('ddd, DD-MMMM-YYYY, HH:mm:ss');
        },

        dateFilterShort(value) {
            if (app.$_.isEmpty(value)) return '-';
            return app.$moment(value).format('DD/MMM/YYYY');
        },

        currencyFilter(val) {
            if (val === null) return '-';
            return formatCurrency(val, {
                symbol: 'IDR ',
                format: '%s%v',
                locale: 'id'
            });
        }
    };

    Object.keys(filters).forEach(v => {
        Vue.filter(v, filters[v]);
    });
};
