import Cookies from 'js-cookie';

export default function (context) {
    if (Cookies.get('r')) {
        context.store.dispatch('authRefresh', {
            data: {
                refresh: Cookies.get('r')
            }
        });
    }

    // context.$axios.setToken(context.store.getters.getUserToken, 'Bearer');

    context.$axios.onRequest(config => {
        if (Cookies.get('t')) {
            config.headers.common.Authorization = `Bearer ${Cookies.get('t')}`;
        }
    });

    // context.$axios.onResponse(res => res.data);

    context.$axios.onResponseError(res => {
        throw res.response === undefined ? { message: res.message } : res.response.data;
        // context.store.dispatch('toggleSnackBar', response.message);
    });
}
